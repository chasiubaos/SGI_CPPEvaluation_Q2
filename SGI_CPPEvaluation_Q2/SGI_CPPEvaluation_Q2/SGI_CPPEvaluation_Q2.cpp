// SGI_CPPEvaluation_Q2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <Windows.h>

int main()
{	
	int count = 0;
	int index = 1;
	int nextNum;
	int k = 1500;
	int factors[3] = { 2, 3, 5 };

	while (count != k)
	{
		nextNum = index;
		for (int i = 0; i < 3; i++)
		{
			while (nextNum%factors[i] == 0)
			{
				nextNum /= factors[i];
			}

			if (nextNum == 1)
			{
				count++;
				printf("%u\n", index);
				break;
			}
		}

		index++;
	}

	system("pause");
    return 0;
}

